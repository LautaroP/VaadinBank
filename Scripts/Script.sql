SELECT distinct pc.cod_capitulo as codCapitulo,
pc.cod_subcapitulo as codSubcapitulo, pc.cod_practi as codPracti,
pc.practi_presta as practiPresta,pc.fec_vigencia as fechaVigencia,
pc.fec_hasta as fechaHasta, pc.diferencial as diferencial,
pc.valoriza as valoriza,np.descrip as descripcion,
pc.practi_presta as nnspm,pc.cod_presta as codPresta,
pc.cod_nomencla as codNomencla, pc.cod_presta_relac as codPrestaRelac

 FROM (presta_contrato pc inner join nomencla_practica np 
 on pc.cod_nomencla = np.cod_nomencla )

inner join 
(select distinct r.cod_presta as aux_cod 
from recepciones r where estado_recep <'2')
on aux_cod=pc.cod_presta

 WHERE pc.cod_capitulo=np.cod_capitulo AND pc.cod_subcapitulo=np.cod_subcapitulo
AND  pc.cod_practi=np.cod_practi AND pc.cod_presta IN ( 13597 )
and pc.practi_presta='340219'


select * from recepciones