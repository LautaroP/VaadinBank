package epidata.banco;

import java.util.Collection;

import pujol.CuentaBancaria;

public interface ICliente {
	public String getNombre();
	public int getDni();
	public void addCuenta(ICuentaBancaria unaCuenta);
	public Collection<CuentaBancaria> getCuentas();
	public void borrarCuenta(ICuentaBancaria cuenta);
}
