package epidata.banco;


public interface ICuentaBancaria{
	
	
	public boolean extraer(double monto);
	public void depositar(double monto);
	
	public ICliente getCliente();
	public double getSaldo();
	public long getNumeroCuenta();
}
