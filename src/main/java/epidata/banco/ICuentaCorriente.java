package epidata.banco;

public interface ICuentaCorriente extends ICuentaBancaria {
	public double getGiroEnDescubierto();
}
