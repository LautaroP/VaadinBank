package designs;

import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import epidata.banco.ICuentaBancaria;
import pujol.Banco;
import pujol.Cliente;
import pujol.CuentaCorriente;
import pujol.CuentaSueldo;
import pujol.MyUI;

public class CuentaShow extends HorizontalLayout{
	
	private Label numeroCuenta= new Label();
	private Label saldo = new Label();
	private Label Tipo = new Label();
	private Label desOJefe=new Label();
	private Button cliente = new Button("Cliente");
	private Button delete = new Button("Borrar Cuenta");
	private Button depositar = new Button("Depositar");
	private Button extraer = new Button("Extraer");
	private HorizontalLayout extDepBtn= new HorizontalLayout(depositar,extraer);
	private MyUI myUI;
	private TextField cantidad=new TextField("Cantidad ");
	private Button confirmar = new Button("Confirmar");
	private Button cancelar = new Button("Cancelar");
	private HorizontalLayout confCancBtn= new HorizontalLayout(confirmar,cancelar);
	private FormLayout formExtDep= new FormLayout(cantidad,confCancBtn);
	private HorizontalLayout botones = new HorizontalLayout(cliente,delete);
	private ICuentaBancaria cuenta;
	private Banco banco= Banco.getInstancia();
	private VerticalLayout layoutCuenta = new VerticalLayout(Tipo,numeroCuenta,saldo,desOJefe,botones,extDepBtn);
	
	public CuentaShow(MyUI myUI) {
		this.myUI=myUI;
		super.addComponents(layoutCuenta,formExtDep);
		depositar.addClickListener(e-> {
			confirmar.addClickListener(e2->depositoConfirmado());
			formExtDep.setVisible(true);
		});
		
		extraer.addClickListener(e-> {
			confirmar.addClickListener(e2->extraccionConfirmado());
			formExtDep.setVisible(true);
		});
		cancelar.addClickListener(e->{
			formExtDep.setVisible(false);
		});
		
		cliente.addClickListener(e->{
			myUI.showCliente((Cliente) cuenta.getCliente());
		});
		delete.addClickListener(e->{
			banco.eliminarCuenta(cuenta);
			setVisible(false);
		});
	}

	private void extraccionConfirmado() {
		if (cuenta.extraer(Double.parseDouble(cantidad.getValue()))) {
			formExtDep.setVisible(false);
			saldo.setValue("Saldo:" + cuenta.getSaldo());
		}
		else {
			new Notification("No puede extraer tanto").show(Page.getCurrent());;
		}

	}


	private void depositoConfirmado() {
		cuenta.depositar(Double.parseDouble(cantidad.getValue()));
		formExtDep.setVisible(false);
		saldo.setValue("Saldo:" + cuenta.getSaldo());
	}


	public void setCuenta(ICuentaBancaria cuenta) {
		this.cuenta=cuenta;
		formExtDep.setVisible(false);
		numeroCuenta.setValue("Numero de Cuenta: " + cuenta.getNumeroCuenta());
		saldo.setValue("Saldo:" + cuenta.getSaldo());
		if (cuenta.getClass()==CuentaCorriente.class) {
			Tipo.setValue("Cuenta Corriente");
			desOJefe.setValue("Descubierto: "+Double.toString(((CuentaCorriente) cuenta).getGiroEnDescubierto()));
		} else {
			Tipo.setValue("Cuenta Sueldo");
			desOJefe.setValue("Empleador: " + ((CuentaSueldo)cuenta).getEmpleador());
		}
		
		
		
	}

}
