package designs;

import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;

import pujol.Banco;
import pujol.Cliente;
import pujol.CuentaInvalidaException;
import pujol.MyUI;
import pujol.NoExisteClienteException;

public class CuentaFormulario extends FormLayout {
	private TextField jefe=new TextField("Jefe: ");
	private TextField descubierto=new TextField("Descubierto: ");
	private FormLayout ccForm= new FormLayout(descubierto);
	private FormLayout csForm= new FormLayout(jefe);
	private Button crear=new Button("Crear Cuenta");
	private TabSheet tipoCuenta = new TabSheet();
	private Banco banco=Banco.getInstancia();
	private MyUI myUI;
	private Cliente cliente;
	
	public CuentaFormulario(MyUI myUI) {
		this.myUI=myUI;
		tipoCuenta.addTab(ccForm, "Cuenta Corriente");
		tipoCuenta.addTab(csForm, "Cuenta Sueldo");
		crear.addClickListener(e -> {
			try {
				if (tipoCuenta.getSelectedTab() == ccForm) {

					banco.nuevaCuenta(cliente.getDni(), Double.parseDouble(descubierto.getValue()));

				} else {
					banco.nuevaCuenta(cliente.getDni(), jefe.getValue());

				}
			} catch (NumberFormatException | NoExisteClienteException | CuentaInvalidaException err) {
				new Notification(err.getMessage()).show(Page.getCurrent());
			}
		});
		addComponents(tipoCuenta,crear);
	}
	public void setCliente(Cliente cliente) {
		this.cliente=cliente;
		tipoCuenta.setSelectedTab(ccForm);
	}
}
