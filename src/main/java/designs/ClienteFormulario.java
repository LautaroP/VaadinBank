package designs;

import com.vaadin.data.Binder;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

import pujol.Banco;
import pujol.Cliente;
import pujol.MyUI;

public class ClienteFormulario extends FormLayout {
	private Label dni = new Label();
	private Banco banco = Banco.getInstancia();
	private TextField nombre = new TextField("Nombre");
	private Button save = new Button("Save");
	private Button delete = new Button("Delete");
	private Button crearCuenta = new Button("Crear Cuenta");
	private Binder<Cliente> binder = new Binder<>(Cliente.class);
	private Cliente clon;
	private MyUI myUI;
	private HorizontalLayout botones = new HorizontalLayout(save, delete, crearCuenta);

	public ClienteFormulario(MyUI myUI) {
		this.myUI=myUI;
		setSizeFull();
		binder.bindInstanceFields(this);
		super.addComponents(nombre,dni,botones);
		save.addClickListener(e->{
			banco.saveClienteModificado(clon);
			myUI.showCliente(clon);
		});
		delete.addClickListener(e-> {
			try {
				banco.deleteCliente(clon.getDni());
			}finally {
				myUI.hideCliente();
			}
			
		});
		crearCuenta.addClickListener(e->{
			myUI.showCuentaForm(clon.getDni());			
		});
	}

	public void setCliente(Cliente cliente) {
		try {
			this.clon = cliente.clone();
		} catch (CloneNotSupportedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (banco.getCliente(clon.getDni()) == null)
			crearCuenta.setVisible(false);
		else {
			crearCuenta.setVisible(true);
		}
		dni.setCaption("Dni: " + clon.getDni());
		binder.setBean(clon);

	}

}
