package pujol;

public class NoExisteClienteException extends Exception {

	private static final long serialVersionUID = 1L;

	NoExisteClienteException(String s) {
		super(s);

	}

}
