package pujol;

import epidata.banco.ICliente;
import epidata.banco.ICuentaSueldo;

public class CuentaSueldo extends CuentaBancaria implements ICuentaSueldo {

	private String empleador;

	public CuentaSueldo(ICliente cliente, String empleador) {
		super(cliente);
		this.empleador = empleador;
	}

	public String getEmpleador() {
		return this.empleador;
	}

	public boolean extraer(double monto) {
		if (super.getSaldo() >= monto) {
			return super.extraer(monto);
		} else {
			return false;
		}

	}

	public String toString() {
		return super.toString() + "\nEmpleador: " + this.getEmpleador();
	}

	public void asignarCuenta(Cliente cliente) {
		cliente.addCuentaSueldo(this);
	}
}
