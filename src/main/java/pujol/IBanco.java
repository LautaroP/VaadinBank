package pujol;

import java.util.Collection;

import epidata.banco.ICuentaBancaria;

public interface IBanco {
	public double getDineroTotal();

	public Cliente getCliente(int dni);

	public boolean nuevoCliente(String nombre, int dni);

	public void nuevaCuenta(int dni, String jefe) throws NoExisteClienteException;

	public void nuevaCuenta(int dni, double descubierto) throws NoExisteClienteException, CuentaInvalidaException;

	public void depositar(int nroCuenta, double monto) throws CuentaInvalidaException;

	public ICuentaBancaria getCuenta(long nroCuenta);

	public Collection<CuentaBancaria> getCuentasCliente(int dni);
}
