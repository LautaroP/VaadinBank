package pujol;

import java.util.Collection;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import designs.ClienteFormulario;
import designs.CuentaFormulario;
import designs.CuentaShow;
import epidata.banco.ICliente;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of an HTML page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Banco banco = Banco.getInstancia();
	private Grid<CuentaBancaria> gridCuentas = new Grid<>(CuentaBancaria.class);
	private HorizontalLayout layoutCliente = new HorizontalLayout();
	private ClienteFormulario ClienteForm= new ClienteFormulario(this);
	private CuentaShow cuentaShow=new CuentaShow(this);
	private TextField dni = new TextField();
	private TextField id = new TextField();
	private Button buscarDni = new Button("Buscar Cliente");
	private Button buscarId = new Button("Buscar Cuenta");
	private CuentaFormulario cuentaNuevaForm=new CuentaFormulario(this);
	@Override
	protected void init(VaadinRequest vaadinRequest) {

		VerticalLayout layout = new VerticalLayout();
		
		dni.setPlaceholder("Dni del Cliente");
		id.setPlaceholder("Id de la Cunta");
		CssLayout buscarCliente = new CssLayout();
		buscarCliente.addComponents(dni,buscarDni);
		buscarCliente.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		CssLayout buscarCuenta = new CssLayout();
		buscarCuenta.addComponents(id,buscarId);
		HorizontalLayout buscador=new HorizontalLayout(buscarCliente,buscarCuenta);
		layoutCliente.addComponents(ClienteForm,gridCuentas);
		layout.addComponents(cuentaShow,layoutCliente,cuentaNuevaForm,buscador);
		hideAll();
		gridCuentas.setColumns("numeroCuenta","saldo");
		gridCuentas.setSizeFull();
		gridCuentas.asSingleSelect().addValueChangeListener(e -> {
			if (e.getValue() != null) {
				showCuenta(e.getValue().getNumeroCuenta());
			}
			layoutCliente.setVisible(false);
		});
		buscarDni.addClickListener(e->{
			showCliente(banco.getCliente(Integer.parseInt(dni.getValue())));			
		});
		buscarId.addClickListener(e-> {
			showCuenta(Long.parseLong((String)id.getValue()));			
		});
		setSizeFull();
		setContent(layout);
	}

	
	public void showCliente(Cliente cliente) {
		if (cliente==null) {
			cliente=new Cliente("", Integer.parseInt(dni.getValue()));
			ClienteForm.setCliente(cliente);
			mostrarClienteNuevo();

		} else {
			updateGridCuentas(cliente);
			ClienteForm.setCliente(cliente);
			mostrarCliente();
		}
	}

	

	private void showCuenta(long l) {
		cuentaShow.setCuenta(banco.getCuenta(l));
		mostrarCuenta();
	}
	

	private void updateGridCuentas(Cliente cliente) {
		Collection<CuentaBancaria> cuentas = banco.getCuentasCliente(cliente.getDni());
		gridCuentas.setItems(cuentas);
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	}

	public void hideCliente() {
		layoutCliente.setVisible(false);		
	}
	private void mostrarCuenta() {
		hideAll();
		cuentaShow.setVisible(true);
		
	}
	private void hideAll() {
		cuentaShow.setVisible(false);
		layoutCliente.setVisible(false);
		cuentaNuevaForm.setVisible(false);
	}


	private void mostrarCliente() {
		hideAll();
		layoutCliente.setVisible(true);
		gridCuentas.setVisible(true);
	}
	private void mostrarClienteNuevo() {
		mostrarCliente();
		gridCuentas.setVisible(false);
	}

	public void showCuentaForm(int dni) {
		cuentaNuevaForm.setCliente(banco.getCliente(dni));
		mostrarCuentaNueva();				
	}


	private void mostrarCuentaNueva() {
		hideAll();
		cuentaNuevaForm.setVisible(true);
	}
}
