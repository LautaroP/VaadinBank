package pujol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import epidata.banco.ICliente;
import epidata.banco.ICuentaBancaria;

public class Cliente implements ICliente ,Serializable, Cloneable  {
	private String nombre;
	private int dni;
	private Collection<CuentaBancaria> cuentas;
	private int cuentasSueldo;

	public Cliente(String nombre, int dni) {
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.cuentas = new ArrayList<CuentaBancaria>();
		this.cuentasSueldo = 0;
	}

	public String getNombre() {
		return this.nombre;
	}
	public Cliente clone() throws CloneNotSupportedException {
		return (Cliente) super.clone();
	}
	

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return this.dni;
	}

	public String toString() {
		return "Cliente: " + getNombre() + " *** DNI: " + getDni();
	}

	@Override
	public boolean equals(Object objeto) {
		if (objeto != null) {
			return ((ICliente) objeto).getDni() == this.getDni()
					&& this.getNombre().equals(((ICliente) objeto).getNombre());
		} else {
			return false;
		}

	}

	public void addCuenta(ICuentaBancaria unaCuenta) {
		if (unaCuenta != null && !cuentas.contains(unaCuenta)) {
			((CuentaBancaria) unaCuenta).asignarCuenta(this);
		} else {
			throw new IllegalArgumentException("Cuenta Invalida");
		}
	}

	public Collection<CuentaBancaria> getCuentas() {
		return this.cuentas;
	}

	public void addCuentaSueldo(CuentaSueldo cuentaSueldo) {
		this.getCuentas().add(cuentaSueldo);
		this.cuentasSueldo++;
	}

	public int cantidadCuentasSueldo() {
		return this.cuentasSueldo;
	}

	public void addCuentaCorriente(CuentaCorriente cuentaCorriente) {
		this.getCuentas().add(cuentaCorriente);
		this.cuentasSueldo--;

	}

	@Override
	public void borrarCuenta(ICuentaBancaria cuenta) {
		cuentas.remove(cuenta);
	}

}
