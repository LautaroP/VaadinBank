package pujol;

import epidata.banco.ICliente;
import epidata.banco.ICuentaCorriente;

public class CuentaCorriente extends CuentaBancaria implements ICuentaCorriente {

	private double descubierto;
	private double deuda;

	public CuentaCorriente(ICliente cliente, double desubierto) {
		super(cliente);
		if (desubierto < 0)
			desubierto = -desubierto;
		this.descubierto = desubierto;
		this.deuda = 0;
	}

	public double getGiroEnDescubierto() {
		return this.descubierto;
	}

	@Override
	public double getSaldo() {
		return super.getSaldo() - this.getDeuda();
	}

	public boolean extraer(double monto) {
		if (super.getSaldo() >= monto) {
			return super.extraer(monto);
		} else {
			if (this.getSaldo() + this.getGiroEnDescubierto() >= monto) {
				this.deuda = monto - this.getSaldo();
				return super.extraer(super.getSaldo());
			}
		}

		return false;
	}

	public String toString() {
		return super.toString() + "\nGiro En Descubierto: $" + this.getGiroEnDescubierto() + " *** Deuda: $"
				+ this.getDeuda();
	}

	public double getDeuda() {
		return this.deuda;
	}

	private double reduceDeuda(double monto) {
		if (monto > 0) {
			this.deuda -= monto;
			if (this.getDeuda() < 0) {
				double aux = -this.getDeuda();
				this.deuda = 0;
				return aux;
			}
			return 0;
		}
		return monto;
	}

	public void depositar(double monto) {
		super.depositar(reduceDeuda(monto));
	}

	public void asignarCuenta(Cliente cliente) {
		if (cliente.cantidadCuentasSueldo() > 0) {
			cliente.addCuentaCorriente(this);
		} else {
			throw new IllegalArgumentException("no pueden haber mas cuentas corrientes q de sueldo");
		}
	}

}