package pujol;

public class CuentaInvalidaException extends Exception {

	public CuentaInvalidaException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
