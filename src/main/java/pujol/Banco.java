package pujol;

import java.util.ArrayList;
import java.util.Collection;

import epidata.banco.ICliente;
import epidata.banco.ICuentaBancaria;
import epidata.banco.ICuentaCorriente;
import epidata.banco.ICuentaSueldo;

public class Banco implements IBanco {
	private Collection<ICliente> clientes;
	private Collection<ICuentaBancaria> cuentas;
	static private Banco instancia = null;

	private Banco() {
		super();
		this.clientes = new ArrayList<ICliente>();
		this.cuentas = new ArrayList<ICuentaBancaria>();
	}
	

	public static Banco getInstancia() {
		if (instancia==null) {
			instancia=new Banco();
			instancia.cargarDatosPrueba();
			
		}
			return instancia;
	}

	private void cargarDatosPrueba() {
		for (int i = 0; i < 100; i++) {
			instancia.nuevoCliente(i + "° Persona", i);
			try {
				instancia.nuevaCuenta( i, "boss");
				instancia.nuevaCuenta( i, i);
			} catch (NoExisteClienteException e) {
				} 
			catch (CuentaInvalidaException e) {
				}
		}

		
	}


	public boolean nuevoCliente(String nombre, int dni) {

		if (this.getCliente(dni) == null) {
			ICliente cliente = new Cliente(nombre, dni);
			this.clientes.add(cliente);
			return true;
		}
		return false;
	}

	public double getDineroTotal() {
		double total = 0;
		for (ICuentaBancaria cuenta : this.cuentas) {
			total += cuenta.getSaldo();
		}
		return total;
	}

	public Cliente getCliente(int dni) {
		for (ICliente cliente : this.clientes)
			if (cliente.getDni() == dni)
				return (Cliente) cliente;
		return null;
	}

	@Override
	public String toString() {
		String s = "";
		for (ICuentaBancaria cuBancaria : cuentas)
			s += cuBancaria.toString() + "\n";
		return s;

	}

	public void nuevaCuenta(int dni, String jefe) throws NoExisteClienteException {
		ICliente cliente = this.getCliente(dni);
		if (cliente != null) {
			ICuentaSueldo cuenta = new CuentaSueldo(cliente, jefe);
			this.cuentas.add(cuenta);
			cliente.addCuenta(cuenta);
		} else {
			throw new NoExisteClienteException("No existe el Cliente");
		}
	}

	public void nuevaCuenta(int dni, double descubierto) throws NoExisteClienteException, CuentaInvalidaException {
		ICliente cliente = this.getCliente(dni);
		if (cliente != null) {
			ICuentaCorriente cuenta = new CuentaCorriente(cliente, descubierto);
			try {
				cliente.addCuenta(cuenta);
			} catch (IllegalArgumentException e) {
				throw new CuentaInvalidaException("no pueden haber mas CC que CS");
			}
			this.cuentas.add(cuenta);

		} else {
			throw new NoExisteClienteException("No existe el Cliente");
		}
	}

	public ICuentaBancaria getCuenta(long nroCuenta) {
		for (ICuentaBancaria cuenta : cuentas) {
			if (cuenta.getNumeroCuenta() == nroCuenta) {
				return cuenta;
			}
		}
		return null;

	}

	public void depositar(int nroCuenta, double monto) throws CuentaInvalidaException {
		ICuentaBancaria cuenta = this.getCuenta(nroCuenta);
		if (cuenta == null)
			throw new CuentaInvalidaException("no existe la cuenta");
		else {
			cuenta.depositar(monto);
		}
	}

	// Metodo para los tests
	public long getLastCuenta() {
		return ((ArrayList<ICuentaBancaria>) this.cuentas).get(cuentas.size() - 1).getNumeroCuenta();
	}

	@Override
	public Collection<CuentaBancaria> getCuentasCliente(int dni) {
		return this.getCliente(dni).getCuentas();
	}


	public void saveClienteModificado(Cliente clon) {
		Cliente cliente = getCliente(clon.getDni());
		if (cliente!=null) {
			clientes.remove(cliente);
		}
		clientes.add(clon);
	}


	public void deleteCliente(int dni) {
		Cliente cliente = getCliente(dni);
		for (ICuentaBancaria cuenta : cliente.getCuentas()) {
			removeCuenta(cuenta);
		}
		clientes.remove(cliente);
		
	}


	private void removeCuenta(ICuentaBancaria cuenta) {
		cuentas.remove(cuenta);		
	}


	public void eliminarCuenta(ICuentaBancaria cuenta) {
		cuenta.getCliente().borrarCuenta(cuenta);
		cuentas.remove(cuenta);
	}

}
