package pujol;

import epidata.banco.ICliente;
import epidata.banco.ICuentaBancaria;

public abstract class CuentaBancaria implements ICuentaBancaria {
	private ICliente cliente;
	private double saldo;
	private static long contador = 0;
	private long nroCuenta;

	public CuentaBancaria(ICliente cliente2) {
		super();
		this.cliente = cliente2;
		this.saldo = 0;
		this.nroCuenta = contador++;

	}

	public boolean extraer(double monto) {
		if (monto >= 0) {
			this.saldo -= monto;
			return true;
		}
		return false;
	}

	public void depositar(double monto) {
		if (monto >= 0)
			saldo += monto;
		else {
			throw new IllegalArgumentException("No se puede depositar un monto negativo");
		}
	}

	public ICliente getCliente() {
		return this.cliente;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public long getNumeroCuenta() {
		return this.nroCuenta;
	}

	public String toString() {
		return "Numero de Cuenta: " + this.getNumeroCuenta() + " *** Saldo: $" + this.getSaldo() + "\n"
				+ this.getCliente();
	}

	public boolean equals(Object objeto) {
		if (objeto != null) {
			ICuentaBancaria cuenta = (ICuentaBancaria) objeto;
			return cuenta.getNumeroCuenta() == this.getNumeroCuenta();
		} else {
			return false;
		}

	}

	public abstract void asignarCuenta(Cliente cliente);
}
